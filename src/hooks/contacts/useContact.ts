import { contactsClient } from '@lib/clients/contacts';
import { IPerson } from '@lib/models/person';
import { useCallback, useEffect, useState } from 'react';

const fetchContact = (id: string) => contactsClient.contact(id);

const useContact = (id: string) => {
  const [contact, setContact] = useState<IPerson | undefined>();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | undefined>(undefined);

  const updateContact = useCallback(async (id: string, name: string, email: string) => {
    setLoading(true);
    try {
      await contactsClient.updateContact(id, name, email);
    } catch (err) {
      const error = err as Error;
      setError(error.message);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    const fetcher = async () => {
      setLoading(true);
      try {
        const response = await fetchContact(id);
        setContact(response);
      } catch (err) {
        const error = err as Error;
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetcher();
  }, []);

  return { contact, loading, error, updateContact };
};

export default useContact;
