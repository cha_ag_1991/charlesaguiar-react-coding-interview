import { faker } from '@faker-js/faker';

import { IPerson, PersonGender } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

function seedContacts(count: number) {
  const res: IPerson[] = [];

  for (let i = 0; i < count; i++) {
    res.push({
      id: faker.datatype.uuid(),
      email: faker.internet.email(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      birthday: faker.datatype.datetime(),
      gender: faker.helpers.arrayElement(Object.values(PersonGender))
    });
  }

  return res;
}

const delay = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

export class MockContactsClient implements IContactsClient {
  private apiContacts: IPerson[];

  constructor(mockPersonsCount: number) {
    this.apiContacts = seedContacts(mockPersonsCount);
  }

  async contactList(opts: IContactListArgs): Promise<IContactListResult> {
    const { pageNumber = 1, pageSize = 10 } = opts;

    await delay(1000);
    const skip = (pageNumber - 1) * pageSize;
    const take = pageSize;

    return {
      data: this.apiContacts.slice(skip, skip + take),
      totalCount: this.apiContacts.length
    };
  }

  async contact(id: string): Promise<IPerson> {
    await delay(1000);
    return this.apiContacts.find(contact => contact.id === id);
  }

  async updateContact(id: string, name: string, email: string): Promise<void> {
    const contact = this.apiContacts.find(contact => contact.id === id);

    if (!contact) {
      throw new Error('Contact does not exist');
    }

    const filteredContacts = this.apiContacts.filter(contact => contact.id !== id);
    this.apiContacts = [...filteredContacts, { firstName: name, email, ...contact }];
  }
}
