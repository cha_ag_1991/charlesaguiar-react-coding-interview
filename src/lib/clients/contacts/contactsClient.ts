import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

export class ContactsClient implements IContactsClient {
  contactList(_opts: IContactListArgs): Promise<IContactListResult> {
    throw new Error('Method not implemented.');
  }

  contact(id: string): Promise<IPerson> {
    throw new Error('Method not implemented.');
  }

  updateContact(id: string, name: string, email: string): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
