import useContact from '@hooks/contacts/useContact';
import { Box } from '@mui/material';
import React, { useRef } from 'react';
import { useParams } from 'react-router-dom';

// import { Container } from './styles';

const ContactEditPage: React.FC = () => {
  const { id } = useParams();
  const { contact, loading, error, updateContact } = useContact(id);
  const nameRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);

  if (!loading || !contact) {
    return <span>Loading...</span>;
  }

  if (error) {
    return <span>Error. Try again.</span>;
  }

  const onSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();

    if (!nameRef.current.value || !emailRef.current.value) {
      alert('Name and Email must not be empty');
    }

    await updateContact(id, nameRef.current.value, emailRef.current.value);
  };

  return (
    <Box>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name</label>
        <input id="name" defaultValue={contact.firstName} ref={nameRef} />
        <label htmlFor="email">Email</label>
        <input id="email" defaultValue={contact.email} ref={emailRef} />
        <button type="submit">Submit</button>
      </form>
    </Box>
  );
};

export default ContactEditPage;
